module gitlab.com/msxq/apps/samztag/v1

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/foolin/pagser v0.1.5
	github.com/go-resty/resty/v2 v2.6.0
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4
	gorm.io/datatypes v1.0.2
)
