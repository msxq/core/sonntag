package main

import (
	"gitlab.com/msxq/apps/samztag/v1/pkg/core"
	"gitlab.com/msxq/apps/samztag/v1/pkg/db"
)

func main() {
	dbConn, _ := db.Connect()
	crawler := core.NewCrawler(dbConn)

	//TODO: Thông tin về doanh nghiệp, mã cổ phiếu
	//crawler.GetCompany()

	//TODO: Thông tin ngành nghề, số lượng CPLH
	//crawler.GetCompanyInfo()

	//TODO: Báo cáo tài chính doanh nghiệp theo quý
	//crawler.GetQuarterReport()

	//TODO: Thiết lập báo cáo tăng trường theo quý.
	//crawler.CreateGrowthReport()

	crawler.CreateQuarterReportFrom()

}
