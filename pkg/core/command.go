package core

type Commander interface {
	Update(cmd string) error

}