package core

const (
	CmdStockUpdate   = "UPDATE_STOCK"
	CmdQuarterUpdate = "UPDATE_QUARTER"
	CmdDailyUpdate   = "UPDATE_DAILY"
	CmdUpdateCompany = "UPDATE_COMPANY"

	SourceUri = "https://ezsearch.fpts.com.vn/Services/EzData/ProcessLoadRuntime.aspx"
	System = "SYSTEM"
)