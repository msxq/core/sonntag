package core

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/foolin/pagser"
	"github.com/go-resty/resty/v2"
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/db"
	"gitlab.com/msxq/apps/samztag/v1/pkg/dtos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/repository"
	"gitlab.com/msxq/apps/samztag/v1/pkg/utils"
	"log"
	"strconv"
	"strings"
)

type Crawler interface {
	GetCompany()
	GetCompanyInfo()
	GetQuarterReport()
	CreateGrowthReport()
	CreateQuarterReportFrom()
}

type crawler struct {
	/*TODO: - Crete connection pool to optimize
	- Update record if exist not create the new one.
	*/
	companyRepository       repository.CompanyRepository
	companyInfoRepository   repository.CompanyInfoRepository
	quarterReportRepository repository.QuarterReportRepository
	growthReportRepository  repository.GrowthReportRepository
	dailyReportRepository   repository.DailyReportRepository

	parser     HTMLParser
	assistants Verifier
}

func NewCrawler(db *db.DB) Crawler {
	cr := repository.NewCompanyRepositories(db)
	cir := repository.NewCompanyInfoRepositories(db)
	qrr := repository.NewQuarterReportRepositories(db)
	grr := repository.NewGrowthReportRepositories(db)
	drr := repository.NewDailyReportRepositories(db)

	p := NewHTMLParser()
	a := NewAssistant(cir)
	return &crawler{
		companyRepository:       cr,
		companyInfoRepository:   cir,
		quarterReportRepository: qrr,
		growthReportRepository:  grr,
		dailyReportRepository:   drr,
		parser:                  p,
		assistants:              a,
	}

}

func (c *crawler) GetCompany() {
	url := "https://liveprice.fpts.com.vn/data.ashx"
	params := map[string]string{
		"s": "company_name",
	}
	headers := map[string]string{
		"Content-Type": "application/json",
	}
	resp := GetResponse(url, params, headers)

	var targets []*daos.Company
	if err := json.Unmarshal(resp.Body(), &targets); err != nil {
		fmt.Printf("could not unmarshal object: %s\n", err)
	}

	for i := 0; i < len(targets); i++ {
		if len(targets[i].Code) < 4 {
			err := c.companyRepository.SaveCompany(targets[i])
			if err != nil {
				fmt.Printf("could not save company %s: %s", targets[i].Code, err)
			}
			continue
		}
	}
}

func (c *crawler) GetCompanyInfo() {
	companies := c.companyRepository.FindAllCompany()
	size := len(companies)

	for i := 0; i < size; i++ {
		cpny := companies[i].ID
		id := strconv.FormatUint(uint64(cpny), 10)
		params := map[string]string{
			"s":      id,
			"cGroup": "Overview",
			"cPath":  "Services/EzData/OverviewProfile",
		}
		headers := map[string]string{
			"Content-Type": "application/x-www-form-urlencoded",
		}

		resp := GetResponse(SourceUri, params, headers)
		content := resp.Body()

		target := daos.CompanyInfo{
			CompanyId: int64(cpny),
		}
		err := c.parser.CrawlCompanyInfo(&target, content)
		if err != nil {
			fmt.Printf("could not crawl company info %s: %s", companies[i].Code, err)
		}

		err = c.companyInfoRepository.SaveCompanyInfo(&target)
		if err != nil {
			fmt.Printf("could not save company info %s: %s", companies[i].Code, err)
		}
	}
	fmt.Println("Done")
}

func (c *crawler) GetQuarterReport() {
	fmt.Println(utils.GetAllPeriod())
	//period := utils.EncodePeriod("1", "2021")
	//c.getQuarterReport(430, period)

}

func (c *crawler) getQuarterReport(companyId int64, period string) error {
	fy, pd := utils.DecodePeriod(period)
	params := map[string]string{
		"s":        strconv.FormatInt(companyId, 10),
		"cGroup":   "Finance",
		"cPath":    "Services/EzData/FinanceReport",
		"ReportID": "Rat",
		"pd":       pd,
		"dt":       "1",
		"fy":       fy,
		"cn":       "1",
		"un":       "1",
	}

	//fmt.Println(params)
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	resp := GetResponse(SourceUri, params, headers)
	content := resp.Body()

	target := daos.QuarterReport{
		CompanyID: 1,
		Period:    period,
	}

	err := c.parser.CrawlQuarterReport(&target, content)
	if err != nil {
		fmt.Printf("could not save company info %s: %s", 430, err)
	}

	err = c.assistants.VerifyQuarterReport(&target)
	fmt.Println(ToJson(target))

	return nil
}

func (c *crawler) CreateGrowthReport() {
	panic("implement me")
}

func (c *crawler) CreateQuarterReportFrom() {
	companies := c.companyRepository.FindAllCompany()
	period := utils.GetAllPeriod()
	fy, pd := utils.DecodePeriod(period[0])

	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}

	for _, v := range companies {
		//for i := 0; i < 1; i++ {
		params := map[string]string{
			"s":        strconv.FormatUint(uint64(v.ID), 10),
			"cGroup":   "Finance",
			"cPath":    "Services/EzData/FinanceReport",
			"ReportID": "Rat",
			"pd":       pd,
			"dt":       "1",
			"fy":       fy,
			"cn":       strconv.Itoa(len(period)),
			"un":       "1",
		}

		resp, err := GetQuarterData(headers, params)
		if err != nil {
			fmt.Printf("failed to get response for %s: %s\n", v.Name, err.Error())
		}

		shares, reports := parseData(resp, v.ID, period)
		c.companyRepository.UpdateCompany(v.ID, shares)
		c.quarterReportRepository.SaveQuarterReports(reports)
		fmt.Printf("crawl data for %s: %s\n", v.Name, v.Code)
	}
}

func parseData(resp *resty.Response, companyId uint, period []string) (int64, []*daos.QuarterReport) {
	p := pagser.New()
	var data dtos.QuarterReportz
	err := p.ParseReader(&data, bytes.NewReader(resp.Body()))
	if err != nil {
		log.Fatal(err)
	}
	var dataz dtos.CollectedData
	dataz.Shares = data.ListedShares.Content
	for _, v := range data.FinancialReportV1 {
		if len(v.Name) == 1 {
			if !strings.EqualFold(strings.ToLower(v.Name[0]), "khả năng trả cổ tức") {
				dataz.Report = append(dataz.Report, dtos.FinancialReport{
					Name:    v.Name[0],
					Content: v.Content,
				})
			}
		}
	}

	//var rawData [][]byte
	//
	//for i, j := range period {
	//	temp := make(map[string]string, 1)
	//	temp["period"] = j
	//	for _, v := range dataz.Report {
	//		temp[v.Name] = v.Content[i]
	//	}
	//	tempJson, _ := json.Marshal(temp)
	//	rawData = append(rawData, tempJson)
	//}

	revenues := initArray(len(period))
	grosses := initArray(len(period))
	nets := initArray(len(period))
	equities := initArray(len(period))
	liabilities := initArray(len(period))
	assets := initArray(len(period))

	var quarters []*daos.QuarterReport

	for _, v := range dataz.Report {
		name := strings.ToLower(v.Name)
		switch name {
		case "doanh thu hoạt động", "doanh thu thuần":
			revenues = convert(v.Content)
			continue
		case "lợi nhuận trước thuế":
			grosses = convert(v.Content)
			continue
		case "lợi nhuận sau thuế":
			nets = convert(v.Content)
			continue
		case "tổng tài sản", "tổng Tài sản":
			assets = convert(v.Content)
			continue
		case "nợ phải trả", "tổng nợ", "tổng nợ phải trả":
			liabilities = convert(v.Content)
			continue
		case "vốn chủ sở hữu", "vốn cổ phần":
			equities = convert(v.Content)
			continue
		default:
			if !isDataNil(revenues, nets, grosses, equities, assets, liabilities) {
				break
			}
			continue
		}
		//fmt.Println(ToJson(quarters))
	}

	for k, v := range period {
		quarters = append(quarters, &daos.QuarterReport{
			CompanyID:   companyId,
			Period:      v,
			Revenue:     revenues[k],
			Gross:       grosses[k],
			Net:         nets[k],
			Equity:      equities[k],
			Liabilities: liabilities[k],
			Assets:      assets[k],
			IsVerified:  false,
			CreatedBy:   System,
		})
	}

	return dataz.Shares, quarters
}

func isDataNil(args ...[]int64) bool {
	for _, v := range args {
		if v == nil {
			return true
		}
	}
	return false
}

func convert(targets []string) []int64 {
	var result []int64
	for _, v := range targets {
		if v == "-" {
			result = append(result, -1)
		} else {
			target := strings.ReplaceAll(v, ",", "")
			if strings.Contains(target, "(") {
				target = strings.ReplaceAll(target, "(", "-")
				target = strings.ReplaceAll(target, ")", "")
			}
			temp, _ := strconv.ParseInt(strings.TrimSpace(target), 10, 64)
			result = append(result, temp)
		}
	}
	return result
}

func initArray(size int) []int64 {
	result := make([]int64, size)
	for k, _ := range result {
		result[k] = -1
	}
	return result
}
