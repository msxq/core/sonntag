package core

import (
	"bytes"
	"encoding/json"
	"github.com/foolin/pagser"
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/dtos"
	"log"
	"strings"
)

type HTMLParser interface {
	CrawlCompanyInfo(target *daos.CompanyInfo, resp []byte) error
	CrawlQuarterReport(target *daos.QuarterReport, resp []byte) error
}

type parser struct {
	parser *pagser.Pagser
}

func NewHTMLParser() HTMLParser {
	p := pagser.New()
	return &parser{
		parser: p,
	}
}

func (p *parser) CrawlCompanyInfo(target *daos.CompanyInfo, resp []byte) error {
	var data dtos.CompanyInfo
	err := p.parser.ParseReader(&data, bytes.NewReader(resp))
	if err != nil {
		return err
	}

	target.ListedShares = data.Shares
	target.Category = data.Category
	return nil
}

func (p *parser) CrawlQuarterReport(target *daos.QuarterReport, resp []byte) error {
	var data dtos.QuarterReport
	err := p.parser.ParseReader(&data, bytes.NewReader(resp))
	if err != nil {
		log.Fatal(err)
	}
	financial := data.FinancialReport
	for i := 0; i < len(financial); i++ {
		name := financial[i].Name
		if strings.Contains(name, "Doanh thu hoạt động") {
			target.Revenue = financial[i].Content
			continue
		}
		if strings.Contains(name, "Lợi nhuận trước thuế") {
			target.Gross = financial[i].Content
			continue
		}

		if strings.Contains(name, "Lợi nhuận sau thuế") {
			target.Net = financial[i].Content
			continue
		}
	}

	balance := data.BalanceReport
	for i := 0; i < len(balance); i++ {
		name := balance[i].Name
		if strings.Contains(name, "Tổng tài sản") {
			target.Assets = balance[i].Content
			continue
		}
		if strings.Contains(name, "Nợ phải trả") {
			target.Liabilities = balance[i].Content
			continue
		}

		if strings.Contains(name, "Vốn chủ sở hữu") {
			target.Equity = balance[i].Content
			continue
		}
	}
	return nil
}

func ToJson(v interface{}) string {
	data, _ := json.MarshalIndent(v, "", "\t")
	return string(data)
}