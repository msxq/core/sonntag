package core

import (
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/repository"
)

type Verifier interface {
	VerifyQuarterReport(report *daos.QuarterReport) error
	CreateGrowthReport(report *daos.GrowthReport) error
}

type Assistants struct {
	CompanyInfoRepository repository.CompanyInfoRepository
}

func (r *Assistants) VerifyQuarterReport(report *daos.QuarterReport) error {

	report.ROE = divide(report.Net, report.Equity) * 100
	report.ROA = divide(report.Net, report.Assets) * 100
	return nil
}

func (r *Assistants) CreateGrowthReport(report *daos.GrowthReport) error {
	panic("not yet implement")
}

func NewAssistant(repository repository.CompanyInfoRepository) Verifier {
	return &Assistants{
		CompanyInfoRepository: repository,
	}
}

func divide(x, y int64) float64 {
	return float64(x) / float64(y)
}