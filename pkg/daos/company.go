package daos

type Company struct {
	ID           uint   `gorm:"autoIncrement"`
	Code         string `gorm:"code"`
	Ex           string `gorm:"ex"`
	Name         string `gorm:"name"`
	ListedShares int64  `gorm:"listed_shares"`
}

type CompanyInfo struct {
	CompanyId int64 `gorm:"company_Id;primaryKey"`

	ListedShares int64  `gorm:"listed_shares"`
	Category     string `gorm:"category"`
	//Description       string `gorm:"description"`
}
