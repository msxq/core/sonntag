package daos

import (
	"gorm.io/datatypes"
)

type QuarterReport struct {
	ID        uint   `gorm:"primaryKey;autoIncrement"`
	CompanyID uint  `gorm:"company_id"`
	Period    string `gorm:"period"`

	Revenue int64 `gorm:"revenue"`
	Gross   int64 `gorm:"gross"`
	Net     int64 `gorm:"net"`

	Equity      int64 `gorm:"equity"`
	Liabilities int64 `gorm:"liabilities"`
	Assets      int64 `gorm:"assets"`

	ROA float64 `gorm:"roa"`
	ROE float64 `gorm:"roe"`

	IsVerified bool   `gorm:"is_verified"`
	CreatedBy  string `gorm:"created_by"`
	UpdatedBy  string `gorm:"updated_by"`

	RawData datatypes.JSON `gorm:"raw_data"`
}

type GrowthReport struct {
	ID        int64  `gorm:"primaryKey;autoIncrement"`
	CompanyID int64  `gorm:"company_id"`
	Period    string `gorm:"period"`

	RevenueGrowth float64 `gorm:"revenue_growth"`
	GrossGrowth   float64 `gorm:"gross_growth"`
	NetGrowth     float64 `gorm:"net_growth"`

	EPSGrowth float64 `gorm:"eps_growth"`
	ROAGrowth float64 `gorm:"roa_Growth"`
	ROEGrowth float64 `gorm:"roe_growth"`
}

type DailyReport struct {
	At        string `gorm:"at"`
	CompanyId int64  `gorm:"company_id"`

	Open   float64 `gorm:"open"`
	Close  float64 `gorm:"close"`
	High   float64 `gorm:"high"`
	Low    float64 `gorm:"low"`
	Volume int64   `gorm:"volume"`

	ForeignBuyVolume  int64 `gorm:"foreign_buy_vol"`
	ForeignBuyValue   int64 `gorm:"foreign_buy_value"`
	ForeignSellVolume int64 `gorm:"foreign_sell_vol"`
	ForeignSellValue  int64 `gorm:"foreign_sell_value"`
}
