package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type GormDB = gorm.DB

func Connect() (*DB, error) {
	db, err := gorm.Open("mysql", "root:admin@tcp(127.0.0.1:3306)/data_stock?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		return nil, err
	}
	db.SingularTable(true)
	return &DB{db}, nil
}

// DB is wrapper of gorm.DB.
type DB struct {
	db *gorm.DB
}

// DB returns current instance of *gorm.DB.
func (_this *DB) DB() *GormDB {
	return _this.db
}

// Begin opens a transaction.
func (_this *DB) Begin() *DB {
	return &DB{_this.db.Begin()}
}

// RollbackUnlessCommitted rollbacks if a transaction not committed.
func (_this *DB) RollbackUnlessCommitted() {
	_this.db.RollbackUnlessCommitted()
}

// Commit closes and saves a DB transaction.
func (_this *DB) Commit() {
	_this.db.Commit()
}
