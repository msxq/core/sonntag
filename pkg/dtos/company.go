package dtos

import (
	"github.com/PuerkitoBio/goquery"
	"strconv"
	"strings"
)

type CompanyInfo struct {
	//Description string `pagser:"#_ctl0_Label30"`
	Category    string `pagser:"#_ctl0_Label32->FormatCategory()"`
	Shares   int64    `pagser:"#_ctl0_Label29->ConvertInt()"`
}

func (i CompanyInfo) ConvertInt(node *goquery.Selection, args ...[]string) (interface{}, error) {
	target := strings.ReplaceAll(node.Text(), ",", "")
	result, _ := strconv.ParseInt(target, 10, 64)
	return result, nil
}

func (i CompanyInfo) FormatCategory(node *goquery.Selection, args ...[]string) (interface{}, error) {
	if node.Text() == "" {
		return "undefined", nil
	}
	return node.Text(), nil
}