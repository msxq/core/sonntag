package dtos

import (
	"github.com/PuerkitoBio/goquery"
	"strconv"
	"strings"
)

type LiveReport struct {
	EPS float32 `json:"eps"`
	ROA float32 `json:"roa"`
	ROE float32 `json:"roe"`
	PE  float32 `json:"pe"`
	PB  float32 `json:"pb"`

	Revenue float64 `json:"revenue"`
	Gross   float64 `json:"gross"`
	Net     float64 `json:"net"`

	RevenueGrowth float64 `json:"revenueGrowth"`
	GrossGrowth   float64 `json:"grossGrowth"`
	NetGrowth     float64 `json:"netGrowth"`

	EPSGrowth float32 `json:"epsGrowth"`
	ROAGrowth float32 `json:"roaGrowth"`
	ROEGrowth float32 `json:"roeGrowth"`
}

type QuarterReport struct {
	FinancialReport []struct {
		Name    string `pagser:"td .report_RowContentBusinessUnit2"`
		Content int64  `pagser:"td .report_RowContentData2->FormatCurrency()"`
	} `pagser:"table #Rat1 table tr"`

	BalanceReport []struct {
		Name    string `pagser:"td .report_RowContentBusinessUnit2"`
		Content int64  `pagser:"td .report_RowContentData2->FormatCurrency()"`
	} `pagser:"table #Rat7 table tr"`

	TitleReport []struct {
		Name    string   `pagser:"td .ReportTitleLeft"`
		Content []string `pagser:"td .reportTitle"`
	} `pagser:"table #t0 tr"`

	FinancialReport2 []struct {
		Name    string   `pagser:"td .report_RowContentBusinessUnit2"`
		Content []string `pagser:"td .report_RowContentData2->FormatCurrency2()"`
	} `pagser:"table #Rat1 table tr"`

	BalanceReport2 []struct {
		Name    string   `pagser:"td .report_RowContentBusinessUnit2"`
		Content []string `pagser:"td .report_RowContentData2->FormatCurrency2()"`
	} `pagser:"table #Rat7 table tr"`

	ListedShares struct {
		Name    string `pagser:"#_ctl0_Label44"`
		Content int64  `pagser:"#_ctl0_lblOS->FormatCurrency()"`
	} `pagser:"table tr td table tr"`
}

func (i QuarterReport) FormatCurrency(node *goquery.Selection, args ...[]string) (interface{}, error) {
	target := strings.ReplaceAll(node.Text(), ",", "")
	if strings.Contains(target, "(") {
		target = strings.ReplaceAll(target, "(", "-")
		target = strings.ReplaceAll(target, ")", "")
	}
	result, _ := strconv.ParseInt(strings.TrimSpace(target), 10, 64)
	return result, nil
}

func (i QuarterReport) FormatCurrency2(node *goquery.Selection, args ...[]string) (interface{}, error) {
	target := strings.ReplaceAll(node.Text(), ",", "")
	if strings.Contains(target, "(") {
		target = strings.ReplaceAll(target, "(", "-")
		target = strings.ReplaceAll(target, ")", "")
	}
	result, _ := strconv.ParseInt(strings.TrimSpace(target), 10, 64)
	return result, nil
}

func FormatCurrency(x string) int64 {
	if x == "-" {
		return 0
	}
	target := strings.ReplaceAll(x, ",", "")
	if strings.Contains(target, "(") {
		target = strings.ReplaceAll(target, "(", "-")
		target = strings.ReplaceAll(target, ")", "")
	}
	result, _ := strconv.ParseInt(strings.TrimSpace(target), 10, 64)
	return result
}

type QuarterReports struct {
	FinancialReport []struct {
		Name    string   `pagser:"td .report_RowContentBusinessUnit2"`
		Content []string `pagser:"td .report_RowContentData2"`
	} `pagser:"table #Rat1 table tr"`

	BalanceReport []struct {
		Name    string   `pagser:"td .report_RowContentBusinessUnit2"`
		Content []string `pagser:"td .report_RowContentData2"`
	} `pagser:"table #Rat7 table tr"`

	ListedShares struct {
		Name    string `pagser:"#_ctl0_Label44"`
		Content int64  `pagser:"#_ctl0_lblOS->FormatCurrency()"`
	} `pagser:"table tr td table tr"`
}

type FinancialReportV1 struct {
	Name    []string `pagser:"td .report_RowContentBusinessUnit2"`
	Content []int64 `pagser:"td .report_RowContentData2->FormatCurrency()"`
}

type QuarterReportz struct {
	FinancialReportV1 []struct {
		Name    []string `pagser:"td .report_RowContentBusinessUnit2"`
		Content []string `pagser:"td .report_RowContentData2"`
	} `pagser:"table tr"`
	ListedShares struct {
		Name    string `pagser:"#_ctl0_Label44"`
		Content int64  `pagser:"#_ctl0_lblOS->FormatCurrency()"`
	} `pagser:"table tr td table tr"`
}

type FinancialReport struct {
	Name    string
	Content []string
}

type QuarterReportzz struct {
	FinancialReportV1 []FinancialReport `json:"financial_report"`
}

type CollectedData struct {
	Report []FinancialReport `json:"financial_report"`
	Shares int64 `json:"shares"`
}

func (i QuarterReports) FormatCurrency(node *goquery.Selection, args ...[]string) (interface{}, error) {
	target := strings.ReplaceAll(node.Text(), ",", "")
	if strings.Contains(target, "(") {
		target = strings.ReplaceAll(target, "(", "-")
		target = strings.ReplaceAll(target, ")", "")
	}
	result, _ := strconv.ParseInt(strings.TrimSpace(target), 10, 64)
	return result, nil
}

func (i QuarterReportz) FormatCurrency(node *goquery.Selection, args ...[]string) (interface{}, error) {
	target := strings.ReplaceAll(node.Text(), ",", "")
	if strings.Contains(target, "(") {
		target = strings.ReplaceAll(target, "(", "-")
		target = strings.ReplaceAll(target, ")", "")
	}
	result, _ := strconv.ParseInt(strings.TrimSpace(target), 10, 64)
	return result, nil
}

