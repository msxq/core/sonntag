package pkg

import (
	"bytes"
	"fmt"
	"github.com/foolin/pagser"
	"github.com/go-resty/resty/v2"
	"gitlab.com/msxq/apps/samztag/v1/pkg/core"
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/dtos"
	"log"
	"strings"

	"strconv"
)

func Example2() {
	client := resty.New()
	client.SetHostURL("https://ezsearch.fpts.com.vn/Services/EzData")

	//target := []int64{12, 185, 214, 1399, 1915, 2053, 2073, 2209, 2211, 2240, 2329, 2343, 2477, 2504, 2521, 2556, 2577, 2578, 2592, 2594, 2595, 2597, 2598, 2599}
	//for i := 0; i < len(target); i++ {
	//	GetSingleDebugMode(target[i], 5, client)
	//}
}

func Example() {
	//dbConn, _ := db.Connect()
	//r := repository.NewCompanyRepositories(dbConn)
	//client := resty.New()
	//client.SetHostURL("https://liveprice.fpts.com.vn")
	//params := map[string]string{
	//	"s": "company_name",
	//}
	//headers := map[string]string{
	//	"Content-Type": "application/json",
	//}
	//
	//resp, _ := client.R().
	//	SetHeaders(headers).
	//	SetQueryParams(params).Get("/data.ashx")
	//
	//fmt.Println(string(resp.Body()))

	client := resty.New()
	client.SetHostURL("https://ezsearch.fpts.com.vn/Services/EzData")
	//ats := utils.GetAllPeriod()

	_, _, _, _, _, _ = GetSingle(12, 5, client)

	//var failed []int64
	//companies := r.FindAllCompany()
	//for index := 0; index < len(companies); index++ {
	//	revenues, grosses, nets, assets, liabilities, equities := GetSingle(companies[index].ID, len(ats), client)
	//
	//	if isDataNil(revenues, nets, grosses, equities, assets, liabilities) {
	//		fmt.Printf("[FAILED] could not get data for %s: %d\n", companies[index].Code, companies[index].ID)
	//		failed = append(failed, companies[index].ID)
	//	} else {
	//		fmt.Printf("> get data for %s: %d\n", companies[index].Code, companies[index].ID)
	//	}
	//}
	//
	//for i := 0; i < len(failed); i++ {
	//	GetSingleDebugMode(failed[i], len(ats), client)
	//}

	//var qrs QRS
	//
	//for i := 0; i < len(ats); i++ {
	//	qrs.Data = append(qrs.Data, daos.QuarterReport{
	//		Period:      ats[i],
	//		Revenue:     dtos.FormatCurrency(revenues[i]),
	//		Gross:       dtos.FormatCurrency(grosses[i]),
	//		Net:         dtos.FormatCurrency(nets[i]),
	//		Equity:      dtos.FormatCurrency(equities[i]),
	//		Liabilities: dtos.FormatCurrency(liabilities[i]),
	//		Assets:      dtos.FormatCurrency(assets[i]),
	//	})
	//}
	//fmt.Println(core.ToJson(qrs))
}

//func GetSingleDebugMode(id int64, cn int, client *resty.Client) {
//	params := map[string]string{
//		"s":        strconv.FormatInt(id, 10),
//		"cGroup":   "Finance",
//		"cPath":    "Services/EzData/FinanceReport",
//		"ReportID": "Rat",
//		"pd":       "2",
//		"dt":       "1",
//		"fy":       "2021",
//		"cn":       strconv.Itoa(cn),
//		"un":       "1",
//	}
//
//	headers := map[string]string{
//		"Content-Type": "application/x-www-form-urlencoded",
//	}
//
//	resp, _ := client.R().
//		SetHeaders(headers).
//		SetQueryParams(params).
//		Get("/ProcessLoadRuntime.aspx")
//
//	p := pagser.New()
//
//	var data dtos.QuarterReportz
//	err := p.ParseReader(&data, bytes.NewReader(resp.Body()))
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	var revenues []string
//	var grosses []string
//	var nets []string
//	var equities []string
//	var liabilities []string
//	var assets []string
//	financial := data.FinancialReport
//	for i := 0; i < len(financial); i++ {
//		name := financial[i].Name
//		switch name {
//		case "Doanh thu hoạt động", "Doanh thu thuần", "Thu nhập lãi và các khoản tương đương thuần", "Tổng doanh thu phí bảo hiểm gốc":
//			revenues = financial[i].Content
//			continue
//		case "Lợi nhuận trước thuế":
//			grosses = financial[i].Content
//			continue
//
//		case "Lợi nhuận sau thuế":
//			nets = financial[i].Content
//			continue
//		case "Tổng tài sản", "Tổng Tài sản":
//			assets = financial[i].Content
//			continue
//		case "Nợ phải trả", "Tổng nợ", "Tổng Nợ", "Tổng nợ phải trả":
//			liabilities = financial[i].Content
//			continue
//		case "Vốn chủ sở hữu", "Vốn cổ phần":
//			equities = financial[i].Content
//			continue
//		default:
//			if !isDataNil(revenues, nets, grosses, equities, assets, liabilities) {
//				break
//			}
//			continue
//		}
//	}
//
//	if revenues == nil {
//		fmt.Printf("revenues data of %d is nil\n", id)
//	}
//	if nets == nil {
//		fmt.Printf("nets data of %d is nil\n", id)
//	}
//	if grosses == nil {
//		fmt.Printf("grosses data of %d is nil\n", id)
//	}
//	if equities == nil {
//		fmt.Printf("equities data of %d is nil\n", id)
//	}
//	if assets == nil {
//		fmt.Printf("assets data of %d is nil\n", id)
//	}
//	if liabilities == nil {
//		fmt.Printf("liabilities data of %d is nil\n", id)
//	}
//}

func GetSingle(id int64, cn int, client *resty.Client) ([]string, []string, []string, []string, []string, []string) {
	var revenues []string
	var grosses []string
	var nets []string
	var equities []string
	var liabilities []string
	var assets []string
	params := map[string]string{
		"s":        strconv.FormatInt(id, 10),
		"cGroup":   "Finance",
		"cPath":    "Services/EzData/FinanceReport",
		"ReportID": "Rat",
		"pd":       "2",
		"dt":       "1",
		"fy":       "2021",
		"cn":       strconv.Itoa(cn),
		"un":       "1",
	}

	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}

	resp, _ := client.R().
		SetHeaders(headers).
		SetQueryParams(params).
		Get("/ProcessLoadRuntime.aspx")

	p := pagser.New()

	var data dtos.QuarterReportz
	err := p.ParseReader(&data, bytes.NewReader(resp.Body()))
	if err != nil {
		log.Fatal(err)
	}
	var dataz []dtos.FinancialReport

	for _, v := range data.FinancialReportV1 {
		if len(v.Name) == 1 {
			if !strings.EqualFold(strings.ToLower(v.Name[0]),"khả năng trả cổ tức") {
				dataz = append(dataz, dtos.FinancialReport{
					Name: v.Name[0],
					Content: v.Content,
				})
			}
		}
	}

	fmt.Println(core.ToJson(data))
	//financial := data.FinancialReport
	//for i := 0; i < len(financial); i++ {
	//	name := financial[i].Name
	//	switch name {
	//	case "Doanh thu hoạt động", "Doanh thu thuần":
	//		revenues = financial[i].Content
	//		continue
	//	case "Lợi nhuận trước thuế":
	//		grosses = financial[i].Content
	//		continue
	//	case "Lợi nhuận sau thuế":
	//		nets = financial[i].Content
	//		continue
	//	case "Tổng tài sản", "Tổng Tài sản":
	//		assets = financial[i].Content
	//		continue
	//	case "Nợ phải trả", "Tổng nợ", "Tổng Nợ", "Tổng nợ phải trả":
	//		liabilities = financial[i].Content
	//		continue
	//	case "Vốn chủ sở hữu", "Vốn cổ phần":
	//		equities = financial[i].Content
	//		continue
	//	default:
	//		if !isDataNil(revenues, nets, grosses, equities, assets, liabilities) {
	//			break
	//		}
	//		continue
	//	}
	//}
	return revenues, grosses, nets, assets, liabilities, equities
}

type QRS struct {
	Data []daos.QuarterReport
}
