package repository

import (
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/db"
)

type CompanyRepository interface {
	SaveCompany(record *daos.Company) error
	FindAllCompany() []daos.Company
	FindCompanyById(id string) daos.Company
	UpdateCompany(id uint, shares int64) error
}

type companyRepository struct {
	connection *db.DB `name:"company"`
}

func (_this *companyRepository) SaveCompany(record *daos.Company) error {
	return _this.connection.DB().Create(&record).Error
}

func (_this *companyRepository) UpdateCompany(id uint, shares int64) error {
	var target daos.Company
	err := _this.connection.DB().First(&target, id).Error
	if err != nil {
		panic(err)
	}
	target.ListedShares = shares
	err = _this.connection.DB().Save(&target).Error
	if err != nil {
		panic(err)
	}
	return nil
}

func (_this *companyRepository) FindAllCompany() []daos.Company {
	var result []daos.Company
	_this.connection.DB().Find(&result)
	return result
}

func (_this *companyRepository) FindCompanyById(id string) daos.Company {
	panic("implement me")
}

func NewCompanyRepositories(db *db.DB) CompanyRepository {
	var repository companyRepository
	repository.connection = db
	db.DB().AutoMigrate(&daos.Company{})
	return &repository
}
