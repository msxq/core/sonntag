package repository

import (
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/db"
)

type CompanyInfoRepository interface {
	SaveCompanyInfo(record *daos.CompanyInfo) error
	FindCompanyInfoById(companyId int64) (daos.CompanyInfo, error)
}

type companyInfoRepository struct {
	connection *db.DB `name:"company_info"`
}

func NewCompanyInfoRepositories(db *db.DB) CompanyInfoRepository {
	var repository companyInfoRepository
	repository.connection = db
	db.DB().AutoMigrate(&daos.CompanyInfo{})
	return &repository
}

func (_this *companyInfoRepository) SaveCompanyInfo(record *daos.CompanyInfo) error {
	//var temp daos.CompanyInfo
	//err := _this.connection.DB().Where("company_id = ?", record.CompanyId).Find(&temp).Error
	//if err == gorm.ErrRecordNotFound {
		return _this.connection.DB().Save(&record).Error
	//} else {
	//	return _this.connection.DB().Model(&temp).Update(&record).Error
	//}
}

func (_this *companyInfoRepository) FindCompanyInfoById(companyId int64) (daos.CompanyInfo, error) {
	var result daos.CompanyInfo
	err := _this.connection.DB().Where("company_id = ?", companyId).Find(&result).Error
	return result, err
}