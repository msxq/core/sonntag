package repository

import (
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/db"
)

type DailyReportRepository interface {
	UpdateReport(record *daos.GrowthReport) error
	FindReportByCompany(code string) error
}

type dailyReportRepository struct {
	connection *db.DB `name:"growth_report"`
}

func (_this *dailyReportRepository) UpdateReport(record *daos.GrowthReport) error {
	panic("implement me")
}

func (_this *dailyReportRepository) FindReportByCompany(code string) error {
	panic("implement me")
}

func NewDailyReportRepositories(db *db.DB) DailyReportRepository {
	var repository growthReportRepository
	repository.connection = db
	db.DB().AutoMigrate(&daos.DailyReport{})
	return &repository
}