package repository

import (
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/db"
)

type GrowthReportRepository interface {
	UpdateReport(record *daos.GrowthReport) error
	FindReportByCompany(code string) error
}

type growthReportRepository struct {
	connection *db.DB `name:"growth_report"`
}

func (_this *growthReportRepository) UpdateReport(record *daos.GrowthReport) error {
	panic("implement me")
}

func (_this *growthReportRepository) FindReportByCompany(code string) error {
	panic("implement me")
}

func NewGrowthReportRepositories(db *db.DB) GrowthReportRepository {
	var repository growthReportRepository
	repository.connection = db
	db.DB().AutoMigrate(&daos.GrowthReport{})
	return &repository
}