package repository

import (
	"gitlab.com/msxq/apps/samztag/v1/pkg/daos"
	"gitlab.com/msxq/apps/samztag/v1/pkg/db"
)

type QuarterReportRepository interface {
	SaveQuarterReport(record *daos.QuarterReport) error
	SaveQuarterReports(record []*daos.QuarterReport) error
	FindReportByCompany(code string) error
}

type quarterReportRepository struct {
	connection *db.DB `name:"quarter_report"`
}

func (_this *quarterReportRepository) SaveQuarterReport(record *daos.QuarterReport) error {
	return _this.connection.DB().Create(&record).Error
}

func (_this *quarterReportRepository) SaveQuarterReports(records []*daos.QuarterReport) error {
	var err error
	for _, v := range records {
		err = _this.SaveQuarterReport(v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (_this *quarterReportRepository) FindReportByCompany(code string) error {
	panic("implement me")
}

func NewQuarterReportRepositories(db *db.DB) QuarterReportRepository {
	var repository quarterReportRepository
	repository.connection = db
	if !db.DB().HasTable(&daos.QuarterReport{}) {
		db.DB().CreateTable(&daos.QuarterReport{})
	}
	return &repository
}

