package utils

import (
	"strconv"
	"strings"
	"time"
)

const (
	BEGIN_YEAR = 2015
)

func EncodePeriod(quarterNr, yearNr string) string {
	return yearNr + "Q" + quarterNr
}

func DecodePeriod(period string) (string, string) {
	result := strings.Split(period, "Q")
	return result[0], result[1]
}

func GetAllPeriod() []string {
	period, year := getPeriodNow()
	var result []string
	if period == 4 {
		for i := year; i >= BEGIN_YEAR; i-- {
			for j := 4; j > 0; j-- {
				temp := strconv.Itoa(i) + "Q" + strconv.Itoa(j)
				result = append(result, temp)
			}
		}
		return result
	} else {
		for i := period - 1; i > 0; i-- {
			temp := strconv.Itoa(year) + "Q" + strconv.Itoa(i)
			result = append(result, temp)
		}

		for i := year - 1; i >= BEGIN_YEAR; i-- {
			for j := 4; j > 0; j-- {
				temp := strconv.Itoa(i) + "Q" + strconv.Itoa(j)
				result = append(result, temp)
			}
		}
		return result
	}
}

func getPeriodNow() (int, int) {
	now := time.Now()
	month := now.Month()

	if 1 <= month && month < 4 {
		return 1, now.Year()
	}

	if 4 <= month && month < 7 {
		return 2, now.Year()
	}

	if 7 <= month && month < 10 {
		return 3, now.Year()
	}

	return 4, now.Year()
}
